package se325.lecture06.examples.helloworld;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class HelloJPAMain {

    private static final Logger LOGGER = LoggerFactory.getLogger(HelloJPAMain.class);

    private static String QUERY = "select m from Message m";

    public static void main(String[] args) {

        EntityManagerFactory entityManagerFactory =
                Persistence.createEntityManagerFactory("se325.lecture06.examples.helloworld");

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        Message message = new Message("Hello, World!");

        entityManager.persist(message);
        entityManager.getTransaction().commit();

        entityManager.getTransaction().begin();
        List<Message> messages = entityManager.createQuery(QUERY, Message.class).getResultList();
        for (Message m : messages) {
            LOGGER.info("Message: " + m);
        }

        messages.get(0).setContent("Take me to your leader!");
        entityManager.getTransaction().commit();
        entityManager.close();

        LOGGER.info("Done!!");
    }

}