package se325.lecture05.jacksonsamples.example07_polymorphism;

import com.fasterxml.jackson.annotation.JsonTypeName;

public class Cat extends Animal {

    public Cat() {
        this(null);
    }

    public Cat(String name) {
        super("Cat", name);
    }

    @Override
    public void sayHello() {
        System.out.println(name + " says Meow!");
    }
}
