package se325.lecture05.jacksonsamples.example04_maps;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class Example04Main {

    public static void main(String[] args) throws IOException {

        PhoneBook phoneBook = new PhoneBook();
        phoneBook.getEntries().put("Alice", new PhoneBookEntry("021 123 4567", "123 Some Street"));
        phoneBook.getEntries().put("Bob", new PhoneBookEntry("021 987 6543", "456 Some Other Street"));

        ObjectMapper mapper = new ObjectMapper();

        String phoneBookJson = mapper.writeValueAsString(phoneBook);

        System.out.println("Phone book json: " + phoneBookJson);

    }

}
