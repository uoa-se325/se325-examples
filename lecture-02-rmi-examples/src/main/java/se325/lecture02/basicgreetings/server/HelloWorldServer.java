package se325.lecture02.basicgreetings.server;

import se325.lecture02.basicgreetings.shared.GreetingService;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class HelloWorldServer {

    public static void main(String[] args) {
        try {
            GreetingService greetingService = new GreetingServiceServant();
            Registry lookupService = LocateRegistry.createRegistry(8080);
            lookupService.rebind("greetingService", greetingService);
            System.out.println("Server up and running!");
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }

}